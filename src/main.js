import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'jquery'
import  'popper.js'
import "@/fontawesome-free-5.9.0-web/css/all.css"
import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();
import '@/styles/style.css'
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
